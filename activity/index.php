<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S04 - Access Modifiers and Encapsulation</title>
</head>
<body>
    
    <h1>Display Information</h1>

    <h2>Person Information</h2>
    <p><?= $person->getName(); ?></p>
    <p><?= $person->getAge(); ?></p>
    <p><?= $person->getAddress(); ?></p>

    <h2>Student Information</h2>
    <p><?= $student->getName(); ?></p>
    <p><?= $student->getAge(); ?></p>
    <p><?= $student->getStudentId(); ?></p>
    <p><?= $student->getAddress(); ?></p>

    <h2>Employee Information</h2>
    <p><?= $job->getName(); ?></p>
    <p><?= $job->getAge(); ?></p>
    <p><?= $job->getTeam(); ?></p>
    <p><?= $job->getRole(); ?></p>
    <p><?= $job->getAddress(); ?></p>

</body>
</html>