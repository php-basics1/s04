<!-- 1. Create multiple classes to demonstrate Access Modifiers and Encapsulation.
    - Reference that will help you for accomplishing the activity:
    https://www.phptutorial.net/php-oop/php-call-parent-constructor/
2. Create a "Person" class with the following properties, this properties must not be accessible outside the class but can be inherited by other classes.
    - name
    - age
    - address
3. Implement a getter and setter methods for each properties of "Person" class.
 -->

<?php 

class Person{
    private $name;
    private $age;
    private $address;

    public function __construct($name, $age, $address)
    {
        $this->name = $name;
        $this->age = $age;
        $this->address = $address;
    }
    
    // Getter(Accessor)
    public function getName(){
        return "Name: $this->name";
    }

    public function getAge(){
        return "Age: $this->age";
    }

    public function getAddress(){
        return "Address: $this->address";
    }

    // Setter(Mutator)
    public function setName($name){
        $this->name = $name;
    }

    public function setAge($age){
        $this->age = $age;
    }

    public function setAddress($address){
        $this->address = $address;
    }

}

// 4. Create a "Student" class that inherits the "Person" class.
// 5. Add a protected property to the Student class for "studentId".
// 6. Implement a getter and setter method for the studentId property.

class Student extends Person {

    protected $studentId;

    public function __construct($name, $age, $studentId, $address){
        parent::__construct($name, $age, $address);

        $this->studentId = $studentId;
    }

    public function getStudentId(){
        return "Student ID: $this->studentId";
    }

    public function setStudentId($studentId){
        $this->studentId = $studentId;
    }
}

// 7. Create an "Employee" class that inherits the "Person" class.
// 8. Add a protected property to the Employee class for "team" and "role".
// 9. Implement a getter and setter method for the "team" and "role" property.

class Employee extends Person {
    protected $team;
    protected $role;

    public function __construct($name, $age, $team, $role, $address){
        parent::__construct($name, $age, $address);
            
            $this->team = $team;
            $this->role = $role;
    }

    public function getTeam(){
        return "Team: $this->team";
    }

    public function getRole(){
        return "Role: $this->role";
    }

    public function setTeam($team){
        $this->team = $team;
    }

    public function setRole($role){
        $this->role = $role;
    }
}

// 10. Create instances of the Person, Student, and Job classes.
// 11. Display each instances information in the browser.

$person = new Person('John Smith', 30, 'Quezon City, Metro Manila');
$student = new Student('Jane Doe', 20, '2023-1980', 'Makati City, Metro Manila');
$job = new Employee('Mark Blain', 35, 'Tech Team', 'Team Lead', 'Pasig City, Metro Manila');